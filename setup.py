from os import path

from setuptools import find_namespace_packages, setup

VERSION = "1.6.3"


this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="NEMO-reports",
    version=VERSION,
    python_requires=">=3.8",
    description="Reports plugin for NEMO",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Atlantis Labs LLC",
    author_email="atlantis@atlantislabs.io",
    url="https://gitlab.com/nemo-community/atlantis-labs/nemo-reports",
    packages=find_namespace_packages(exclude=["NEMO_reports.tests", "NEMO_reports.tests.*"]),
    include_package_data=True,
    install_requires=[
        "django",
        "python-dateutil",
    ],
    extras_require={
        "NEMO-CE": ["NEMO-CE"],
        "NEMO": ["NEMO>=4.5.0"],
    },
    license="MIT",
    keywords=["NEMO"],
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Framework :: Django :: 3.2",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
    ],
)
